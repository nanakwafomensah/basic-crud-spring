package com.example.demo.service;

import com.example.demo.entities.Student;
import com.example.demo.repos.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Student saveStudent(Student student) {
       return  studentRepository.save(student);

    }

    @Override
    public Student updateStudent(int studentId , Student student) {
       Student selectStudent= studentRepository.getOne(studentId);
       selectStudent.setName(student.getName());
       selectStudent.setCourse(student.getCourse());
       selectStudent.setFee(student.getFee());
       return  studentRepository.save(selectStudent);
    }

    @Override
    public void deleteStudent(int studentId) {
        studentRepository.deleteById(studentId);
    }

    @Override
    public List<Student> allstudent() {
        return studentRepository.findAll();
    }

    @Override
    public Student getStudentById(int id) {
       Student student =studentRepository.getOne(id);
       return student;

    }
}
