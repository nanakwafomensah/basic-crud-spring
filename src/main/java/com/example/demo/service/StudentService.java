package com.example.demo.service;

import com.example.demo.entities.Student;

import java.util.List;
import java.util.Optional;

public interface StudentService {
    Student saveStudent(Student student);
    Student updateStudent(int studentId, Student student);
    void deleteStudent(int studentId);
    List<Student> allstudent();
    Student getStudentById(int id);
}
