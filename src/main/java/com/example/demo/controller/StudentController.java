package com.example.demo.controller;

import com.example.demo.entities.Student;
import com.example.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.jws.WebParam;


@Controller
public class StudentController {

    @Autowired
    private StudentService studentService;

    @RequestMapping(value="/students",method = RequestMethod.GET)
    public String allstudent(Model model){
        model.addAttribute("students",studentService.allstudent());
        return "students/allstudent";
    }
    @RequestMapping(value="/students/create",method=RequestMethod.POST)
    public ModelAndView create(@RequestParam("name") String name, @RequestParam("course") String course, @RequestParam("fee") double fee){
            Student student =new Student(name,course,fee);
            studentService.saveStudent(student);
        return new ModelAndView("redirect:/students");
    }

    @RequestMapping(value = "/students/{id}/delete",method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable int id){
        studentService.deleteStudent(id);
        return new ModelAndView("redirect:/students");
    }

    @RequestMapping(value="/students/update",method=RequestMethod.POST)
    public ModelAndView update(@RequestParam("id") int id,@RequestParam("name") String name, @RequestParam("course") String course, @RequestParam("fee") double fee){
        studentService.updateStudent(id,new Student(name,course,fee));
        return new ModelAndView("redirect:/students");
    }
    @RequestMapping(value = "/students/{id}/edit",method = RequestMethod.GET)
    public String edit(@PathVariable int id, Model model){
        model.addAttribute("student",studentService.getStudentById(id));
        return "students/editstudent";
    }

}
